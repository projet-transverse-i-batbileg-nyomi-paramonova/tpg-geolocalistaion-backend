const {assert, expect} = require('chai');
const geo = require('../geoUtils');
const rewire = require('rewire');
const algo = rewire('../algo');
const fs = require('fs');
const { compute_thermometer } = require('../algo');

// TODO: Create test based on demo data

describe('#inGeneva()', () => {
    // FIXME: arrow functions are actually bad practice in mocha tests as it stops from using keyword this for things like timeouts for example
    it('should return true if in Geneva', () => {
        assert.equal(geo.inGeneva({
            lat: 46.1764,
            lng: 6.1401
        }), true)
    });
})

describe('#distance()', () => {
    it('should return the shortest distance on a sphere for the Statue of liberty and the Eiffel Tower', () => {
        assert.equal(Math.round(geo.distance({lat: 40.6892, lng: -74.0444}, {lat: 48.8583, lng: 2.2945})), 5837);
    });
})


describe('#deg_to_rad', () => {
    it('should convert degrees to radians', () => {
        assert.equal(geo.deg_to_rad(45), 0.7853981633974483);
    });
})

describe('#bus_line', () => {
    it.skip('should find the correct line for the given test user', () => {
        
    })
})

describe('#averagePositions', () =>{
    it('should calculate the average position from the given positions', () =>{
        assert.deepEqual(algo.averagePositions([{lat:46.224141,lng: 6.112406},{lat:46.223738, lng:6.111808}]), {lat: 46.2239395, lng: 6.112107 });
    });
})

describe('#averageSpeed', () =>{
    var averageSpeed;
    before(() => {
        averageSpeed = algo.__get__('averageSpeed');
    });


    it('should calculate the average speed from the given speeds', () =>{
        assert.equal(averageSpeed([1, 0]), 0.5);
        assert.equal(averageSpeed([1, 1]), 1);
    });
    
    it('should return 0 when the inputs are all 0', () => {
        assert.equal(averageSpeed([0, 0]), 0);
    });
    
    it('should return 0 when the given array is empty' , () => {
        // Testing what happens if array is empty
        assert.equal(averageSpeed([]), 0); // TODO: Check if this is really the expected behavior..  or error message ?
    });
})

describe('#calculateArrivalTimeFromPosition', () =>{
    var averageSpeed;
    before(() => {
        averageSpeed = algo.__get__('averageSpeed');
    });
    it('should calculate the time it takes to get between 2 positions using an average velocity', () =>{
        const vel = averageSpeed([2,3,5,6.7]);
        assert.deepEqual(algo.calculateArrivalTimeFromPosition({lat:46.224141,lng: 6.112406},{lat:46.223738, lng:6.111808},vel), 0.25637367865089195);
    });
    
    it('should retturn Infinity given zero velocity', () => {
        expect(algo.calculateArrivalTimeFromPosition({lat:46.224141,lng: 6.112406},{lat:46.223738, lng:6.111808}, 0)).to.be.equal(Infinity);
    })
})

describe('#updateThermo', () =>{
    var originalThermo;
    var updatedThermo;
    beforeEach(() =>  {
        originalThermo = JSON.parse(fs.readFileSync("testThermo.json"));
        updatedThermo = algo.updateThermo(9,originalThermo);
        
    });
    it('should not return an undefined thermometer', () => {
        expect(updatedThermo.steps).to.not.be.undefined;
    });

    it('should update a given thermometer', () =>{
        // Tests that array is actually modified and also immutability
        expect(updatedThermo).to.not.deep.equal(originalThermo);
    });
    
    it('should reduce all arrival times with the given inputs', () => {
        for (let [i, step] of updatedThermo.steps.entries()){
            expect(step.arrivalTime).to.not.be.undefined;
            expect(step.arrivalTime).to.be.below(+originalThermo.steps[i].arrivalTime);
        }
        
    })

})

describe('#compute_thermometer', () => {
    var originalThermo;
    beforeEach(() => {
        originalThermo = JSON.parse(fs.readFileSync("testThermo.json"));
    });
    it('should return the original thermometer given no position or velocity input', () => {
        // Returns undefined might be an expected behavior but is dangerous since method user needs to check whether defined
        expect(compute_thermometer(null, [], originalThermo, [])).to.deep.equal(originalThermo);
        
    });

    it('should return the original thermometer given no position and 0 velocities as input', () => {
        // Returns undefined might be an expected behavior but is dangerous since method user needs to check whether defined
        expect(compute_thermometer(null, [], originalThermo, [0,0,0,0])).to.deep.equal(originalThermo);        
    });
    it('should return a thermometer with infinite arrival times for non moving objects', () => {
        // Returns undefined might be an expected behavior but is dangerous since method user needs to check whether defined
        const updatedThermo = compute_thermometer([{lat: 46.224141, lng: 6.112406}, {lat: 46.223738, lng: 6.111808}], [], originalThermo, []);
        updatedThermo.steps.forEach(step => {
            expect(step.arrivalTime).to.eql(Infinity);
        });
    });
})
