### A clean backend based on websockets

In order to run the backend simply use 

```
npm install
npm start
```

If you want to upload more data it needs to be in a folder called `google_data_takeout`. You can then edit for the script for the month you want to upload and run.

```
node populate-db.js
```

This will upload data to the database with the given data. It might be necessary to define a new user. Xoeseko Used id 1 but you can use anything during the test phase it isn't that important.

If you want to create an "ephemeral" user on the phpmyadmin web service run

```SQL
INSERT INTO `utilisateur`(id_utilisateur, bus_voulu) VALUES (2, 12) 
```

To create a user 2 trying to take the tram 12 for example.

### Viewing
In order to view the map run the frontend in a separate terminal with the command :

```
npm install 
npm start
```

The frontend connects to this backend server using the socket.io implementation of websockets.