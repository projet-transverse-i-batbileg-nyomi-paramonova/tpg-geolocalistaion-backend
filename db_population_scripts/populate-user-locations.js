if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'gitlab'){
    require('dotenv').config();
}
const fs = require('fs');
const mysql = require('mysql');
con = mysql.createConnection({
    host: process.env.DB_HOSTNAME || 'localhost',
    port: process.env.DB_PORT || 3306,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
})

con.query("SELECT * FROM utilisateur", (err, res) => {
    if (err) {
        console.log(err)
    } else {
        console.log(res)
    }
});

const month = JSON.parse(fs.readFileSync('./google_data_takeout/Takeout/Location History/Semantic Location History/2019/2019_NOVEMBER.json'));

const places = month.timelineObjects.map( obj => {
    // This class regroups places where the user was present for a longer period of time shouldn't be useful for the algorithmm
    if(Object.keys(obj).includes("placeVisit")){
        return {
            lat: obj.placeVisit.location.latitudeE7 * 10**(-7),
            lng: obj.placeVisit.location.longitudeE7 * 10**(-7),
            timestamp: obj.placeVisit.duration.startTimestampMs
        };
    }
}).filter(place => place);

// console.table(places);

for (const p of places){
    query = `INSERT INTO \`vecteur_etat\`(\`latitude\`, \`longitude\`, \`temps_enregistrement\`, \`id_enregistreur\`) VALUES(${p.lat}, ${p.lng}, FROM_UNIXTIME(${p.timestamp * 0.001}), 1)`;
    // console.log(query)
    con.query(query)
    con.commit()
}
console.log('Finished inserts');