const fs = require('fs');
if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'gitlab'){
    require('dotenv').config();
}
const mysql = require('mysql');
con = mysql.createConnection({
    host: process.env.DB_HOSTNAME || 'localhost',
    port: process.env.DB_PORT || 3306,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
})

// stream = fs.createReadStream('./CSV_TPG_ARRETS/TPG_ARRETS.csv', {
//     encoding: 'utf-8',
// });
stops = []
createdLines = new Set()


JSON.parse(fs.readFileSync('./tpg_arret.json')).stops.forEach( stop => {
    
    for (ps of stop.physicalStops) {
        queries = []
        query1 = `INSERT INTO arret_bus(id_arret, nom_courant, latitude, longitude) VALUES("${ps.physicalStopCode}", "${ps.stopName}", ${ps.coordinates.latitude}, ${ps.coordinates.longitude})`;
        // console.log(query1);
        queries.push(query1);

        for (connection of ps.connections){
            if(!(connection.lineCode in createdLines)) {
                query3 = `INSERT IGNORE INTO ligne SET numero_ligne="${connection.lineCode}"`
                queries.push(query3);
                createdLines.add(connection.lineCode)
            }
            // FIXME: Il semblerait qu'utiliser les conections soit peut-être trop large... Exemple Balex ou le 18 et le 10 ont une connection...
            query2 = `INSERT INTO ligne_possede_arret(ligne, arret, destination) VALUES("${connection.lineCode}", "${ps.physicalStopCode}", "${connection.destinationCode}")`
            queries.push(query2)
        }

        for (q of queries) {
            con.query(q);
            con.commit()
        }

        // con.query()
        // stops.push({
        //     id: ps.physicalStopCode,
        //     name: ps.stopName,
        //     lat: ps.coordinates.latitude,
        //     lng: ps.coordinates.longitude,
        //     connections: ps.connections,
        // });
        
    }
});

// doneDestinations = new Set();
// for (stop of stops){
//     for (connection of connections){
//         query1 = `INSERT INTO ligne_possede_arret(ligne, arret) VALUES (${connection.lineCode}, ${stop.id})`;
//         console.log(query1);
//         // TODO: Adapt second query to use ST_Endpoint() mysql function in order to check that the endpoint is the desired stop and edit the stop name in `ligne`
//         if(!doneDestinations.has(connection.lineCode+connection.destinationCode)){
//             query2 = `INSERT INTO ligne_possede_arret(ligne, arret) VALUES (${connection.lineCode}, ${stop.id})`;
//             doneDestinations.add(connection.lineCode+connection.destinationCode);
//         }
//     }
// }
