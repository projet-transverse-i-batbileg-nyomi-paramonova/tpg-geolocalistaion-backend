if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'gitlab'){
    require('dotenv').config();
}
const fs = require('fs');
const mysql = require('mysql');
let con = mysql.createConnection({
    host: process.env.DB_HOSTNAME || 'localhost',
    port: process.env.DB_PORT || 3306,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
})

// lineMap = {};

// JSON.parse(fs.readFileSync('./tpg_arret.json')).stops.forEach( stop => {
    
//     for (ps of stop.physicalStops) {
//         // query1 = `INSERT INTO arret_bus(id_arret, nom_arret, latitude, longitude) VALUES('${ps.physicalStopCode}', '${ps.stopName}', ${ps.coordinates.latitude}, ${ps.coordinates.longitude})`;
//         // console.log(query1);
//         // con.query()
//         for (connection of ps.connections){
//             if(connection.destinationName == stop.stopName && (!(connection.lineCode + connection.destinationName) in lineMap)){
//                 lineMap[connection.lineCode + connection.destinationName] = stop.physicalStopCode;
//             }
//         }
        
//     }
// });

const features = JSON.parse(fs.readFileSync('tpg_lignes_ok.geojson'));
const lignes = features.features.map( obj => {
    multilineString = []
    obj.geometry.coordinates.forEach(pointList => {
        multilineString.push('LineString(Point(' + pointList.join('), Point(') + '))')
    })

    //console.log(multilineString)
    //if((obj.properties.LIGNE + obj.properties.DIRECTION) in lineMap) return {
    return{    
        nomLigne : obj.properties.LIGNE,
        direction : obj.properties.DIRECTION,
        // direction : lineMap[obj.properties.LIGNE + obj.properties.DIRECTION],
        parcours : multilineString,
    }
    //}
}).filter( ligne => ligne )

console.table(lignes);

for (const l of lignes){
    query = `INSERT INTO \`trace\`(\`ligne\`, \`direction\`, \`parcours\`) VALUES('${l.nomLigne}', '${l.direction}', MultilineString(${l.parcours}))`;
    // console.log(query)

    con.query(query, (err, res) => {
        if (err) {
            console.log(err);
        }
    } )
    con.commit()
}

// con.close();
// console.log('Finished inserts');