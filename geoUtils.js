const EARTH_RADIUS_KM = 6371;
const GENEVA = {
    lat: 46.1984,
    lng: 6.1423
};

distance = (p1, p2) => {
    lat1 = deg_to_rad(p1.lat)
    lng1 = deg_to_rad(p1.lng)
    lat2 = deg_to_rad(p2.lat)
    lng2 = deg_to_rad(p2.lng)
    
    return Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lng1-lng2)) * EARTH_RADIUS_KM
}

deg_to_rad = (deg) => {
    return deg * (Math.PI/180);
}

// Returns true if the distance is smaller than 16 KM from the center of geneva should be wide enough
// TODO: Test some edge cases
inGeneva = (p1) => {
    return distance(p1, GENEVA) < 16
}

module.exports = {
    distance,
    deg_to_rad,
    inGeneva,
}