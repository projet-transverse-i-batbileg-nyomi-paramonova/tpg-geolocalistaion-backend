const fs = require('fs');
const geoUtils = require('./geoUtils');

// Entry : New User position, Other positions, Thermometer, userInfo, Other Users Velocity
// New User ->  position : { lat: '', lng: '' }
// Other positions : [ position, positions, ... ]
// Thermometer : Json Object Thermometer from TPG
// Velocities : [ velocity, velocity ] : velocity -> m/s or null if not available

function algo(newPosition, positions, thermo, velocities) {

    if(!newPosition){
        console.log("The new position is invalid returning original thermometer");
        return thermo;
    }

    const adjusted_position = averagePositions([...positions, newPosition]);
    const velocity = averageSpeed(velocities);
    const at = calculateArrivalTimeFromPosition(adjusted_position, positions, velocity);

    return updateThermo(at, thermo);
}

function averagePositions(positions) { // positions = [ position, positions ]
    const averagePosition = {
        lat: 0,
        lng: 0
    };

    for (const position of positions) {

        averagePosition.lat = averagePosition.lat + position.lat;
        averagePosition.lng = averagePosition.lng + position.lng;
    }

    averagePosition.lat = averagePosition.lat / positions.length;
    averagePosition.lng = averagePosition.lng / positions.length;

    return averagePosition;

}

function calculateArrivalTimeFromPosition(position, stopPos, velocity) {
    if(velocity){
        const distance = geoUtils.distance(position, stopPos) * 1000; // meters 
        return (distance / velocity) / 60; // change to minutes
    } else {
        // Returning infinity so that object should be static if velocity is 0;
        return Infinity;
    }

}

function averageSpeed(velocities) {
    if(Array.isArray(velocities) && velocities.length)
        return velocities.reduce((tot, vel) => tot + vel, 0) / velocities.length; // in m/s
    else return 0;
}

function updateThermo(arrivalTime, thermo) {
    // The original method which was modifying all objects in the array was modified in order to avoid side effects like changing the original array
    const deltaTime = arrivalTime - thermo.steps[0].arrivalTime;
    // The method copies the thermometer into a new object before mapping the entries in steps into entries with modified times
    return {
        ...thermo, 
        steps: thermo.steps.map(step => (
            {
                ...step, 
                arrivalTime: +step.arrivalTime + deltaTime
            }) 
        )
    }
}

module.exports = {
    averagePositions,
    calculateArrivalTimeFromPosition,
    updateThermo,
    compute_thermometer: algo
}