const fs = require('fs');
const geo = require('../geoUtils');

if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'gitlab') {
    require('dotenv').config();
}
const mysql = require('mysql');

con = mysql.createConnection({
    host: process.env.DB_HOSTNAME || 'localhost',
    port: process.env.DB_PORT || 3306,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
})

START_TIME = '2020-03-30T10:00:00'

con.query('SELECT * FROM trace WHERE ligne LIKE \'18\'', (err, res) => {
    if (err) {
        console.log(err);
    } else {
        // console.log(res[0]);
        const user_segs = res[0].parcours.map(seg => {
            var t = Date.now();
            // var prev_v = 0;
            var v = 0;
            return seg.map((pos, id,) => {
                if (id > 0) {
                    // prev_v = v;
                    v = Math.random() * (25 - 9) + 9;
                    const distance_delta = geo.distance({ lat: seg[id - 1].y, lng: seg[id - 1].x }, { lat: pos.y, lng: pos.x });
                    // console.log(distance_delta);
                    if (v != 0)
                        t += ((distance_delta * 1000) / v); // Distance divided by time (1) times the speed or distance travelled in one unit A.K.A seconds / 60
                }
                // console.log(Date(t));
                return {
                    lat: pos.y,
                    lng: pos.x,
                    velocite: v, // 10 meters per second is approximately 35 kmh
                    temps_enregistrement: Date(t)
                }
            })
        })
        for (let id = 0; id < user_segs.length; id++){
            for (vector of user_segs[id]){
                con.query(`INSERT IGNORE INTO vecteur_etat(latitude, longitude, velocite, id_enregistreur, temps_enregistrement) VALUES (${vector.lat}, ${vector.lng}, ${vector.velocite}, 'test', DATE('${vector.temps_enregistrement}'))`, err =>{
                  if(err)
                    console.log(err)
                });
            }
            con.commit();
        }
    }

    con.end();
});
