if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'gitlab') {
    require('dotenv').config();
}
var http = require('http');

function getDepartureCodes(physicalstopcode, lineCode, destinationCode, stopCode) // get departureCode from these 4 arguments
{
    var url = 'http://prod.ivtr-od.tpg.ch/v1/GetNextDepartures?stopCode=' + stopCode + '&key=' + process.env.TPG_KEY;
    //console.log(url);
    return new Promise((resolve, reject) => { // return a promise whether it is resolved or rejected

        http.get(url, (res) => {
            let data = '';

            res.on('data', (chunk) => {
                data += chunk;
            });
            res.on('end', () => {
                var fbResponse = JSON.parse(data);
                // console.log(data);
                const departureCodes = fbResponse.departures.map(obj => { // for each json object check if conditions apply
                    // console.log(physicalstopcode, lineCode, destinationCode);
                    // if(Object.keys(obj).includes("departureCode") && obj.mnemol == physicalstopcode && obj.line.lineCode == lineCode && obj.line.destinationCode == destinationCode){
                    if (obj.departureCode !== undefined && obj.mnemol == physicalstopcode && obj.line.lineCode == lineCode && obj.line.destinationCode == destinationCode) {
                        // console.log('This object will be returned', obj.departureCode);
                        return +obj.departureCode; // push to array
                    }
                }).filter(code => code);
                // console.log(`Getting information for ${lineCode}, ${physicalstopcode}, ${destinationCode}`)
                // console.log(departureCodes);
                // console.log(departureCodes.length);
                // console.log(departureCodes[0]);
                resolve({first: departureCodes[0], last: departureCodes[departureCodes.length -1]}); // get the first one
            });

        }).on("error", (err) => {
            reject("Error: " + err.message);
        });
    })
    // test Variables
    // var physicalstopcode = 'WTC00'
    // var lineCode = '10';
    // var destinationCode = 'RIVE';
    // var stopCode = 'WTC0'
}

function getThermometer(departureCode, finalCode) {
    var url = 'http://prod.ivtr-od.tpg.ch/v1/GetThermometerPhysicalStops?departureCode=' + departureCode + '&key=' + process.env.TPG_KEY;
    return new Promise((resolve, reject) => {

        http.get(url, (res) => {
            let data = '';

            res.on('data', (chunk) => {
                data += chunk;
            });

            res.on('end', () => {
                console.log(url);
                // console.log(data);
                const fbResponse = JSON.parse(data);
                //console.log(fbResponse);
                // console.log(fbResponse);
                const dataJson = fbResponse.steps.map(obj => {
                    if (Object.keys(obj).includes('arrivalTime')) { // check if there is an arrivaltime because sometimes there isn't any
                        // console.log(obj);
                        var dataToPush = {
                            coordinates: obj.physicalStop.coordinates,
                            stopCode: obj.stop.stopCode,
                            physicalStopCode: obj.physicalStop.physicalStopCode,
                            arrivalTime: obj.arrivalTime
                        };
                        return dataToPush
                    }
                })
                resolve(dataJson); // resolve it
            });

        }).on("error", (err) => {
            reject("Error: " + err.message);
        });
    })
}

// How to use function

// getDepartureCode('WTC00','10','RIVE','WTC0').then(response =>{
//     console.log(`departurecode > ${response}`);
//     getThermometer(response).then(resp =>{
//         console.log(resp);
//     }).catch(err =>{
//         console.log(err);
//     })
// }).catch(err =>{
//     console.log(err);
// })

//


getTimetables = (physicalStopCode, lineCode, DestinationCode, stopCode) => {
    //console.log(`Getting timetable for \n line : ${lineCode} \n destination : ${DestinationCode} \n from : ${physicalStopCode}`);
    return new Promise((resolve, reject) => {

        getDepartureCodes(physicalStopCode, lineCode, DestinationCode, stopCode).then(response => {
            console.log(`departurecode > ${response.first}`); // to see departureCode
            console.log(`finalCode     > ${response.last}`); // to see departureCode
            // FIXME: Don't check inexistent departure codes
            if (response) {
                getThermometer(response.first).then(resp => {
                    // console.log(resp); //see timetable for a specific line, direction and a starting stop  
                    resolve({nextStops: resp, code: response.last});
                }).catch(err => {
                    reject(err);
                })
            }
        }, rejection => {
            console.log(rejection);
        }).catch(err => {
            reject(err);
        })
    });
}



module.exports = {
    getTimetables
}

