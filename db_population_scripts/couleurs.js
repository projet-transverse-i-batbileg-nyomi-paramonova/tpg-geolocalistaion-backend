require('dotenv').config();
const http = require('http');


const url = 'http://prod.ivtr-od.tpg.ch/v1/GetLinesColors?key=' + process.env.TPG_KEY;
const mysql = require('mysql');
const con = mysql.createConnection({
    host: process.env.DB_HOSTNAME || 'localhost',
    port: process.env.DB_PORT || 3306,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
})

http.get(url, res => {
    let data = ''

    res.on('data', chunk => {
        data += chunk
    });

    res.on('end', () => {
        // console.log(process.env.TPG_KEY);
        // console.log(data);
        JSON.parse(data).colors.forEach(col => {
            const query = `UPDATE ligne SET couleur = '${col.hexa}' WHERE numero_ligne LIKE '${col.lineCode}'`;
            console.log(query);
            con.query(query);
        })
        con.end()
    })
}).on('error', err => {
    console.log(err);
});