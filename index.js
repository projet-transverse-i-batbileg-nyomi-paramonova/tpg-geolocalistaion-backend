const express = require('express');
const mysql = require('mysql');
const socketIO = require('socket.io');
const http = require('http');
const timetable = require('./timetable-method');
const geo = require('./geoUtils');
const { compute_thermometer } = require('./algo');
const Rx = require('rxjs');

if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'gitlab') {
    require('dotenv').config();
}

const app = express();

const server = http.createServer(app);
const io = socketIO(server);

const con = mysql.createConnection({
    host: process.env.DB_HOSTNAME || 'localhost',
    port: process.env.DB_PORT || 3306,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
})

bus_map = {};
bus_map$ = new Rx.BehaviorSubject({});

io.on('connection', socket => { // server automatically listens for "connection" messages in order to start a connection with a new client.
    console.log(`New connection from client given id: ${socket.id}`);

    getLigns().then(ligns => { // Getting all the lines and creating an endpoint
        socket.emit('lines', ligns);
    });

    // socket.on('new-position', (position) => { // Defining a new position message to listen on
    //     console.log(`Client ${socket.id} currently at position ${position.lat}, ${position.lng}`);
    //     sendPositionToDb(position, socket.id);
    //     // socket.broadcast.emit('position', position);
    //     // TODO: Check for non existent ID
    // });

    socket.on('get-lines', stop => { // getting all lines for a stop and creating an endpoint
        getLignsForStop(stop).then(ligns => {
            socket.emit(stop, ligns);
        });
    });

    socket.on('get-stops', line => { // get all stops for a line and create an endpoint
        console.log("Stops for line", line);
        getStopsForLine(line).then(positions => {
            socket.emit(line, positions.map(row => {
                return {
                    name: row.nom_courant,
                    lat: row.latitude,
                    lng: row.longitude,
                    id: row.id_arret,
                    destination: row.destination,
                }
            }));
        }, err => console.log(err));
    });

    socket.on('get-road', line => {
        getPreciseRoad(line).then(coords => {
            socket.emit('road', coords);
        });
    });



    socket.on('get-next-positions', (physicalStopCode, lineCode, destinationCode) => {
        getTimetables(physicalStopCode, lineCode, destinationCode.slice(0, -1)).then(res => {
            console.log(res);
        },err =>{
            console.log(err);
        });
    });

    socket.on('get-destination', (stopCode, line) => {
        con.query(`SELECT id_arret FROM arret_bus INNER_JOIN ligne_possede_arret WHERE ligne_possede_arret.arret LIKE '${stopCode}' AND ligne_possede_arret.ligne LIKE '${line}'`,
            (err, res) => {
                if (err) {
                    console.log(err);
                }
                else {
                    socket.emit('destination', res);
                }
            }
        )
    })

    socket.on('get-demo', () => {

        demo_positions().then(positions => {
            for (row of positions) {
                const pos = {
                    lat: row.latitude,
                    lng: row.longitude,
                    timestamp: row.temps_enregistrement
                }
                if (geo.inGeneva(pos)) {
                    socket.emit('position', pos);
                }
            };
        }, err => console.log(err));

    })

    socket.on('disconnect', () => {
        console.log(`Client ${socket.id} disconnected`)
    })
});

con.query('SELECT * FROM arret_bus JOIN ligne_possede_arret JOIN ligne WHERE ligne.numero_ligne=ligne_possede_arret.ligne AND ligne_possede_arret.arret = arret_bus.id_arret GROUP BY numero_ligne, arret, destination', (err, res) => {
    if (err) {
        console.log(err);
    } else {

        const startInfo = res.map(record => {
            // FIXME: Secondary effect of map is undesired
            const nsp = io.of(`/${record.numero_ligne}/${record.nom_courant.replace(/\s/g, '')}/${record.destination.replace(/\s/g, '')}`);
            nsp.on('connection', socket => {
                handleStopConnection(socket, record.numero_ligne, record.nom_courant, record.destination, record.arret);
                console.log('connection of', socket.id);
                // nsp.on('new-position', (position) => {
                //     const id = socket.id.split('#')[1];
                //     console.log(position);
                //     getPositionsForBusId(bus_map[id].bus_id).subscribe(old_pos_vel => {
                //         console.log(old_pos_vel);
                //         compute_thermometer(position, old_pos_vel, bus_map[id].thermo,).subscribe(thermo => {
                //             console.log(thermo);
                //             bus_map[id].thermo = thermo;
                //             socket.emit(bus_map[id].bus_id, thermo);
                //         });

                //     })
                //     // handleNewUserPosition(id, record.line, record.stop, record.destination, position);
                // });
            });
            
            return {
                ligne: record.numero_ligne,
                arret: record.arret,
                nom_arret: record.nom_courant,
                destination: record.destination,
                socket: `/${record.numero_ligne}/${record.nom_courant.replace(/\s/g, '')}/${record.destination.replace(/\s/g, '')}`
            }
        });
        
        io.on('connection', socket => {
            socket.emit('start', startInfo);
            socket.on('new-position', (position) => {
                console.log(socket.id);
                const id = socket.id
                // .split('#')[1];
                console.log(position);
                bus_map$.subscribe(bm => {
                    if (id in bm) {
                        getPositionsForBusId(bm[id].bus_id).then(old_pos_vel => {
                            console.log(old_pos_vel);
                            const new_thermo = compute_thermometer(position, old_pos_vel, bm[id].thermo, [])
                            bm[id].thermo = new_thermo;
                            io.of(bm[id].socket).emit('next-stops', new_thermo);
                            console.log(new_thermo);
                        });
                    }
                });
                // handleNewUserPosition(id, record.line, record.stop, record.destination, position);
            });
        });
    }
})

server.listen(process.env.PORT || 3000, () => console.log(`websocket server listening on port ${process.env.PORT || 3000}`))


function getPositionsForBusId(id) {
    return new Promise((resolve, reject) => {
        con.query(`SELECT latitude as lat, longitude as lng, velocite as velocity FROM vecteur_etat INNER JOIN utilisateur ON id_utilisateur=id_enregistreur WHERE bus_voulu=${id}`, (err, records) => {
            if(err){
                console.log(err);
                reject(err);
            } else {
                resolve(records);
            }
        });

    });
}


// TODO: Query only relevant values and define functions in another file for filtering

demo_positions = () => {
    return new Promise((resolve, reject) => {
        con.query('SELECT * FROM vecteur_etat', (err, res) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                resolve(res);
            }
        });
    })
}

getStops = () => {
    console.log('responding to get-stops');
    return new Promise((resolve, reject) => {
        con.query({
            // Ugly query to get the unique stops
            sql: 'SELECT * FROM arret_bus WHERE (nom_courant, id_arret) IN (SELECT nom_courant, MIN(id_arret) FROM arret_bus GROUP BY nom_courant)',
            // debug: true,
        }, (err, res) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                resolve(res);
            }
        })
    })
}

getLignsForStop = (stop) => {
    return new Promise((resolve, reject) => {
        con.query(`SELECT DISTINCT numero_ligne, couleur FROM ligne INNER JOIN arret_bus WHERE id_arret LIKE "${stop.slice(0, -2)}%"`, (err, rows) => {
            if (err) reject(err);
            else resolve(rows);
        });
    });
}

getLigns = () => {
    return new Promise((resolve, reject) => {
        con.query(`SELECT * FROM ligne`, (err, rows) => {
            if (err) reject(err);
            else resolve(rows);
        });
    });
}

getStopsForLine = (line) => {
    return new Promise((resolve, reject) => {
        con.query(`SELECT * FROM ligne JOIN ligne_possede_arret JOIN arret_bus WHERE ligne.numero_ligne=ligne_possede_arret.ligne AND ligne_possede_arret.arret = arret_bus.id_arret AND ligne.numero_ligne LIKE '${line}'`, (err, rows) => {
            if (err) reject(err);
            else resolve(rows);
        });
    });
}

getPreciseRoad = (line) => {
    return new Promise((resolve, reject) => {
        con.query(`SELECT direction, ST_AsGeoJSON(parcours) as coords  FROM trace WHERE ligne LIKE "${line}"`, (err, res) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                resolve(res[0])
                // console.log(res);
            }
        })
    });
}




sendPositionToDb = (p, id) => {


    // FIXME: doesn't always work it sometimes get's a NaN field... Enabled debug now we still need to run the frontend with debug active
    const query = `INSERT INTO \`vecteur_etat\`(\`latitude\`, \`longitude\`, \`temps_enregistrement\`, \`id_enregistreur\`) VALUES(${p.lat}, ${p.lng}, FROM_UNIXTIME(${p.timestamp * 0.001}), '${id || 1}')`;
    // console.log(query)

    con.query({
        sql: query,
        debug: true,
    }, (err, res) => {
        if (err)
            console.log(err);
    })
    con.commit()
}

createUser = (id, wanted_bus) => {
    const query = `INSERT INTO utilisateur (id_utilisateur, bus_voulu) VALUES ('${id}', ${wanted_bus}) ON DUPLICATE KEY UPDATE bus_voulu=${wanted_bus}`;

    con.query({
        sql: query,
        debug: true,
    }, (err) => {
        if (err) console.log(err);
    })
}

function handleStopConnection(socket, line, stop, destination, stopcode) {
    // console.log(stop, line, destination, stop.slice(0,4));
    timetable.getTimetables(stopcode, line, destination, stopcode.slice(0, 4)).then(res => {
        const nextStops = res.nextStops.filter(one => one);
        const id = socket.id.split('#')[1];
        // The individual bus id is considered to be the departure code at terminus since this is available the whole way
        if (!bus_map[id]) {
            bus_map[id] = {};
        }
        bus_map[id]['socket'] = socket.id.split('#')[0];
        bus_map[id]['bus_id'] = res.code;
        bus_map[id]['thermo'] = nextStops;
        bus_map$.next(bus_map);
        createUser(id, bus_map[id].bus_id);
        socket.emit('next-stops', nextStops);
    });
}

// function handleNewUserPosition(socket, line, stop, destination, stopcode, thermo) { // 
//     //console.log(line,stop,destination,stopcode);
//     //console.log("Thermo: ", thermo);

//     // testing Mode using json

//     con.query('', res, err => {



//     });





//     // query positions that are significant for algorithm
//     // if position of 1 user at least then proceed else return updated thermo from TPG
//     // if new position then agregate with average
//     // the new position transform it to time using MRU
// }
